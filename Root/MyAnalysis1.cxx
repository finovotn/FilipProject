#include <MyAnalysis/MyAnalysis.h>
#include <AsgTools/MessageCheck.h>
#include <vector>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
#include  <xAODMuon/MuonContainer.h>

//---- tracking
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"


const xAOD::TrackParticle *MaxPtTrack;
// this is needed to distribute the algorithm to the workers
ClassImp(MyAnalysis)

#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )

MyAnalysis :: MyAnalysis ():
   h_booked(false),MeV2GeV(0.001)
{
	
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.
}


EL::StatusCode MyAnalysis:: setupJob (EL::Job& job)
{
	// Here you put code that sets up the job on the submission object
	// so that it is ready to work with your algorithm, e.g. you can
	// request the D3PDReader service or add output files.  Any code you
	// put here could instead also go into the submission script.  The
	// sole advantage of putting it here is that it gets automatically
	// activated/deactivated when you add/remove the algorithm from your
	// job, which may or may not be of value to you.
	job.useXAOD ();
	//job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);

	EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file


	return EL::StatusCode::SUCCESS;
}


EL::StatusCode MyAnalysis :: histInitialize ()
{
	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected.
	FillVertices();
	FillTracks();
	h_booked = true;
	return EL::StatusCode::SUCCESS;
}


EL::StatusCode MyAnalysis:: fileExecute ()
{
	// Here you do everything that needs to be done exactly once for every
	// single file, e.g. collect a list of all lumi-blocks processed


	return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyAnalysis:: changeInput (bool firstFile)
{
	// Here you do everything you need to do when we change input files,
	// e.g. resetting branch addresses on trees.  If you are using
	// D3PDReader or a similar service this method is not needed.

	if( firstFile ){}

	m_event = wk()->xaodEvent();
	Info("initialize()", "Number of events = %lli", m_event->getEntries() ); 
	Info("initialize()", "Number of events = %lli", m_event->getEntries() ); 

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode MyAnalysis :: initialize ()
{
	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected.
	return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyAnalysis :: execute ()
{
	// Here you do everything that needs to be done on every single
	// events, e.g. read input variables, apply cuts, and fill
	// histograms and trees.  This is where most of your actual analysis
	// code will go.
//	m_goodVertices.clear;
	Alltracks = 0;
	Allvertices = 0;
	max = 0.00;
	MaxPtTrack = nullptr;
	const xAOD::TrackParticleContainer* tracks(0);
		m_event->retrieve( tracks, "InDetTrackParticles" ).ignore(); // TODO not ignore??? 

	for(const auto trk : *tracks) {
		std::cout << "Track: " << trk->pt() << std::endl;  
		std::cout << " " <<  trk->eta()
			<<  trk->phi() << " " <<  trk->d0() << " " <<  trk->z0()
			<< std::endl;
		if (max < trk->pt()){
			MaxPtTrack = trk; 
			max = trk->pt();
		}

	}
	std::cout << "ntracks: " <<tracks->size() << std::endl;
	Alltracks  += tracks -> size ();
//	std::cout << "MAX: " <<max << std::endl;
	if (MaxPtTrack != nullptr){
	std::cout << "MAX: " <<MaxPtTrack -> pt() << std::endl;
	}

	const xAOD::VertexContainer *vertices(0);
  	m_event->retrieve( vertices, "PrimaryVertices" ).ignore();
      std::vector<xAOD::Vertex*> m_goodVertices; //!
 	for(auto vtx : *vertices ) { //loop over vertices in the container
	Allvertices  += vertices -> size ();	

    // --- fill all selected vertices
    if ( vtx->vertexType() == xAOD::VxType::VertexType::PriVtx
        ||  vtx->vertexType() == xAOD::VxType::VertexType::PileUp ) m_goodVertices.push_back( vtx );		
   

}
	FillTracks();
	FillVertices();
	return EL::StatusCode::SUCCESS;

}


EL::StatusCode MyAnalysis:: postExecute()
{
	// Here you do everything that needs to be done after the main event
	// processing.  This is typically very rare, particularly in user
	// code.  It is mainly used in implementing the NTupleSvc.
	return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyAnalysis :: finalize ()
{
	// This method is the mirror image of initialize(), meaning it gets
	// called after the last event has been processed on the worker node
	// and allows you to finish up any objects you created in
	// initialize() before they are written to disk.  This is actually
	// fairly rare, since this happens separately for each worker node.
	// Most of the time you want to do your post-processing on the
	// submission node after all your histogram outputs have been
	// merged.
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyAnalysis:: histFinalize ()
{
	// This method is the mirror image of histInitialize(), meaning it
	// gets called after the last event has been processed on the worker
	// node and allows you to finish up any objects you created in
	// histInitialize() before they are written to disk.  This is
	// actually fairly rare, since this happens separately for each
	// worker node.  Most of the time you want to do your
	// post-processing on the submission node after all your histogram
	// outputs have been merged.  This is different from finalize() in
	// that it gets called on all worker nodes regardless of whether
	// they processed input events.
	return EL::StatusCode::SUCCESS;
}


void MyAnalysis::FillTracks() {

std::cout << h_booked << std::endl;
	if( !h_booked){
		const float pi = 3.141592613;
		Book( new TH1F("htrkPt", "htrkPt; p_{T} [GeV]; tracks", 200, 0, 100));
		Book( new TH1F("htrkEta", "htrkEta; #eta; tracks", 50, -2.5, 2.5));
		Book( new TH1F("htrkPhi", "htrkPhi; #phi; tracks", 50, pi, pi));
		Book( new TH1F("MAX", "MAX; p_{T} [GeV]; event", 200, 0, 100));
		Book( new TH1F("TRACKS", "TRACKS; count; event", 200, 0, 100));
		return;
	}


	const xAOD::TrackParticleContainer* tracks(0);
	m_event->retrieve( tracks, "InDetTrackParticles" ).ignore();


	for(const auto trk : *tracks) {
		hist("htrkPt")-> Fill(trk-> pt()*MeV2GeV);
		hist("htrkPhi")-> Fill(trk-> phi());
	}

	xAOD::TrackParticleContainer::const_iterator trk_it     = tracks-> begin();
	xAOD::TrackParticleContainer::const_iterator trk_end   = tracks-> end();

	for(; trk_it!= trk_end; trk_it++){

		hist("htrkEta")-> Fill( (*trk_it)-> eta());
	}
	if (MaxPtTrack != nullptr){
		hist("MAX")->Fill(MaxPtTrack ->pt()*MeV2GeV);
	} 
	if (Alltracks != 0 ){
		hist("TRACKS")->Fill(Alltracks);
	}
	else{
		hist("TRACKS")->Fill(0);
	}
}
void MyAnalysis::FillVertices() {
		if(!h_booked){
		Book( new TH1F("verZ", "verZ; mm; tracks", 200, -100, 100));
		Book( new TH1F("Count", "Count; count; event", 200, 0, 100));
		return;
	}
	const xAOD::VertexContainer *vertices(0);
  	m_event->retrieve( vertices, "PrimaryVertices" ).ignore();
for( auto vtx : *vertices ) {
		hist("verZ")-> Fill(vtx->z());
		}
	if (Allvertices != 0 ){
		hist("Count")->Fill(Allvertices);
	}
	else{
		hist("Count")->Fill(0);
	}

}
