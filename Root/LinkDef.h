#ifndef MYANALYSIS_MYANALYSIS_DICT_H
#define MYANALYSIS_MYANALYSIS_DICT_H

// This file includes all the header files that you need to create
// dictionaries for.

#include <MyAnalysis/MyAnalysis.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class MyAnalysis+;
#endif

#endif
