#include "MyAnalysis/Dilepton.h"



Dilepton::Dilepton(xAOD::IParticle*const a,xAOD::IParticle*const b):leading(a),subleading(b){
}

double Dilepton::mass() {
return (((leading->p4()+subleading->p4()).M())*0.001);
}
