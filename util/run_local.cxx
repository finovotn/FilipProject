#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/ScanDir.h"

//--- needed for ntuple output definition
#include "EventLoopAlgs/NTupleSvc.h" 
#include "EventLoop/OutputStream.h"
#include "EventLoopGrid/PrunDriver.h"

//--- EXCLRunII includes
#include "MyAnalysis/MyAnalysis.h"


using namespace std;

int main( int argc, char* argv[] ) {

   
    // Take the submit directory from the input if provided:
    std::string submitDir = "subDir3";
    if( argc > 1 ) submitDir = argv[ 1 ];



    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    // Construct the samples to run on:
    SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:

//   const char* inputFilePath = gSystem->ExpandPathName ("/home/nechansky/work/ep/files/mc16a_testfile");
  //const char* inputFilePath = gSystem->ExpandPathName ("/eos/user/o/openc/hssip_samples/mc/mc16_13TeV.300000.Pythia8BPhotospp_A14_CTEQ6L1_pp_Jpsimu2p5mu2p5.deriv.DAOD_BPHY4.e3989_s3126_r9364_r9315_p3263");
  //const char* inputFilePath = gSystem->ExpandPathName ("/eos/user/o/openc/hssip_samples/mc/mc16_valid.300100.Pythia8BPhotospp_A14_CTEQ6L1_pp_Upsilon1S_mu4mu4.merge.AOD.e4397_s2997_r9370_r9315");
  const char* inputFilePath = gSystem->ExpandPathName ("/eos/user/o/openc/hssip_samples/data/data15_13TeV.00276262.physics_Main.merge.DAOD_STDM3.r7562_p2521_p3298");

  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/user/f/finovotn/hssip_samples/mc/mc16_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.deriv.DAOD_STDM3.e3601_s3126_r9364_r9315_p3371");
  // const char* inputFilePath = gSystem->ExpandPathName ("/tmp/okepka/hssip_samples/mc/mc16_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.deriv.DAOD_STDM3.e3601_s3126_r9364_r9315_p3371");


    SH::ScanDir().filePattern("*").scan(sh, inputFilePath);

     // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString( "nc_tree", "CollectionTree" );

    // Print what we found:
    sh.print();


    EL::Job job;
    job.sampleHandler( sh );
    job.options()->setDouble (EL::Job::optMaxEvents, -1); //--- for testing only
  // job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);
    job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);

  //---to prevent long processing time for MC15b AOD's (signal)
//  if( sampleName.find(".AOD.e4717_s2726_r7326_r6282") == std::string::npos ) job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);
    // define an output and an ntuple associated to that output
    EL::OutputStream output  ("myOutput");
    job.outputAdd (output);
 //   EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  //  job.algsAdd (ntuple);

    // Add our analysis to the job:
    // // //  MyxAODAnalysis* alg = new MyxAODAnalysis();
    // //   EXCLRunIIAnalysis *alg = new EXCLRunIIAnalysis;
    // //   // later on we'll add some configuration options for our algorithm that go here
    // //   job.algsAdd( alg );
    // // 
    // //   alg->outputName = "myOutput"; // give the name of the output to our algorithm

    //  MyxAODAnalysis* alg = new MyxAODAnalysis();
//    ExclWW_init *alg_init = new ExclWW_init();
//        job.algsAdd( alg_init );
//    alg_init->m_hasTrk = true;
    //alg_init->outputName = "ExclWW_init"; // give the name of the output to our algorithm

    MyAnalysis *alg_analysis = new MyAnalysis();
    job.algsAdd( alg_analysis );
   // alg_analysis->outputName = "ExclWW_analysis"; // give the name of the output to our algorithm

      // Run local:
    // Run the job using the local/direct driver:
    EL::DirectDriver driver;
   driver.submit( job, submitDir );

    return 0;
}
