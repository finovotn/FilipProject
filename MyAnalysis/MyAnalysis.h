#ifndef MyAnalysis_MyAnalysis_H
#define MyAnalysis_MyAnalysis_H

#include <EventLoop/Algorithm.h>


// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

// EDM includes:
#include "xAODTracking/Vertex.h"
#include "xAODEventInfo/EventInfo.h"
#include <TH1F.h>
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
 #include "xAODEgamma/ElectronContainer.h"
 #include "xAODEgamma/ElectronAuxContainer.h"
#include <xAODJet/JetContainer.h>

class MyAnalysis : public EL::Algorithm
{
   public:
      // this is a standard algorithm constructor
      //MyAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
      MyAnalysis ();

      // these are the functions inherited from Algorithm
      virtual EL::StatusCode setupJob (EL::Job& job);
      virtual EL::StatusCode fileExecute ();
      virtual EL::StatusCode histInitialize ();
      virtual EL::StatusCode changeInput (bool firstFile);
      virtual EL::StatusCode initialize ();
      virtual EL::StatusCode execute ();
      virtual EL::StatusCode postExecute ();
      virtual EL::StatusCode finalize ();
      virtual EL::StatusCode histFinalize ();


   private:


      template<class T>
         void Book( T *h ) 
         {
            h->Sumw2();
            book ( *h );
         }
      int Alltracks; 
      int AllMuons;
      int AllElectrons;
      float Allvertices;
      float max;
      float MeV2GeV; 
      void FillTracks();
      void FillVertices();
      void FillMuons();
      void FillEle();
      bool m_isMC;
      bool h_booked;
      // event variables
      xAOD::TEvent* m_event; //!
      const xAOD::EventInfo* m_eventInfo; //!
      const xAOD::MuonContainer* muons; //!
      std::vector<xAOD::Vertex*> m_goodVertices; //!
      std::vector<const xAOD::Muon*> m_goodMuons; //!
      std::vector<const xAOD::Electron*> m_goodElectrons; //!
      const xAOD::ElectronContainer* electrons = 0; //!
      ClassDef(MyAnalysis, 1);
};

#endif
