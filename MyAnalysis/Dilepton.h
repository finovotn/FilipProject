#include "xAODBase/IParticle.h"
class Dilepton {
const xAOD::IParticle* leading;
const xAOD::IParticle* subleading;
public: 
double mass();
//user must fill constructor, that a has higher pt than b
Dilepton(xAOD::IParticle*const a,xAOD::IParticle*const b);
}; 

